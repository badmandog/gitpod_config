#    This file is part of the Converter distribution.
#    Copyright (c) 2021 whishfox
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3.
#
#    This program is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#    General Public License for more details.
#
#    License can be found in < https://github.com/kruzira/converter/blob/main/License> .

from . import *

try:
    APP_ID = 6800960
    API_HASH = '868583386066479a0b4b801d3653dc0b'
    BOT_TOKEN = "5288596237:AAGyY9eUADmD-sn5uDNyVDMEjgi1HMhx1UI"
    OWNER = config("OWNER_ID", default=957902327, cast=int)
    LOG = config("LOG_CHANNEL", default=957902327, cast=int)
except Exception as e:
    LOGS.info("Environment vars Missing")
    LOGS.info("something went wrong")
    LOGS.info(str(e))
    exit(1)
